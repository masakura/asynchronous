# .NET の非同期ベンチマーク

## 手順
```shell
ansible-playbook virtualmachines.yml
ansible-playbook webapps.yml
ansible-playbook monitoring.yml
ansible-playbook k6.yml
```

データベースを作らせる。

```shell
k6$ ulimit -n 65536
k6$ k6 run default.js  --out influxdb=http://monitoring:8086/benchmark
```

Grafana にデータソースを追加。

* Prometheus
* InfluxDB (benchmark)

Grafana ダッシュボードをインポート。

* [Node Exporter Full](https://grafana.com/grafana/dashboards/1860)
* [k6 Load Testing Results](https://grafana.com/grafana/dashboards/2587)
* [benchmark.json](monitoring/benchmark.json)

ベンチマークを取る。

```shell
# 最小スレッド数デフォルト
k6$ k6 run default.js  --out influxdb=http://monitoring:8086/benchmark
# 非同期
k6$ k6 run async.js  --out influxdb=http://monitoring:8086/benchmark
# 同期 (最小スレッド数 10,000)
k6$ k6 run sync.js  --out influxdb=http://monitoring:8086/benchmark
```
