import http from 'k6/http';
import { check } from 'k6';
import opts from './options.js';

export const options = opts;

export default () => {
    const res = http.get('http://localhost:5167/async');
    check(res, { 'status was 200': r => r.status == 200 });
}