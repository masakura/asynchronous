import http from 'k6/http';
import { check } from 'k6';
import opts from './options.js';

export const options = opts;

export default () => {
    const res = http.get('http://sync/sync');
    check(res, { 'status was 200': r => r.status == 200 });
}