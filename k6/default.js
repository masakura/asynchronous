import http from 'k6/http';
import { check } from 'k6';
import opts from './options.js';

export const options = {
    vus: 100,
    duration: '10s',
};

export default () => {
    const res = http.get('http://default/sync');
    check(res, { 'status was 200': r => r.status == 200 });
}