namespace TargetApp.Threads;

internal static class ThreadsExtensions
{
    public static void InitializeThreadPool(this IServiceCollection services)
    {
        services.AddHostedService<ThreadPoolInitializer>();
    }
}