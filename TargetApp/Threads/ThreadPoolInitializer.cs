using Microsoft.Extensions.Options;

namespace TargetApp.Threads;

internal sealed class ThreadPoolInitializer : IHostedService
{
    private readonly ILogger<ThreadPoolInitializer> _logger;
    private readonly IOptions<ThreadPoolOptions> _options;

    public ThreadPoolInitializer(IOptions<ThreadPoolOptions> options, ILogger<ThreadPoolInitializer> logger)
    {
        _options = options;
        _logger = logger;
    }

    private ThreadPoolOptions Options => _options.Value;

    public Task StartAsync(CancellationToken cancellationToken)
    {
        if (Options.MinWorkerThreads > 0)
        {
            ThreadPool.GetMinThreads(out _, out var completionPortThreads);
            ThreadPool.SetMinThreads(Options.MinWorkerThreads, completionPortThreads);
        }

        ShowInformation();

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    private void ShowInformation()
    {
        ThreadPool.GetMinThreads(out var workerThreads, out var completionPortThread);
        _logger.LogInformation(
            $"MinWorkerThreads => {workerThreads}, MinCompletionPortThread => {completionPortThread}");
    }
}