namespace TargetApp.Threads;

internal sealed class ThreadPoolOptions
{
    // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
    public int MinWorkerThreads { get; set; } = 0;
}