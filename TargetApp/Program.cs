using TargetApp.Threads;

var builder = WebApplication.CreateBuilder(args);
builder.Services
    .Configure<ThreadPoolOptions>(builder.Configuration.GetSection("ThreadPool"))
    .InitializeThreadPool();

var app = builder.Build();

app.MapGet("/sync", () =>
{
    Thread.Sleep(2500);
    return "Non async";
});
app.MapGet("/async", async () =>
{
    await Task.Delay(2500);
    return "async";
});
app.MapGet("/", async () =>
{
    await Task.Yield();
    return "Hello World!";
});

app.Run();